
$('.switch input').change(function() {
    $(this).closest('.switch').find('.switch_label').removeClass('checked');
    $(this).closest('.switch_label').addClass('checked');
});

$('.scope').raty({
    number      : 5,
    half        : true,
    starType    : 'i',
    score       : function() {
        return $(this).attr('data-number');
        },
    readOnly    : function() {
        return $(this).attr('data-readOnly');
        }

});


var gallery = new Swiper('.place__slider_container', {
    slidesPerView: 3,
    spaceBetween: 12,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        1200: {
            slidesPerView: 1,
            spaceBetween: 10
        }
    }
});



var slider = new Swiper('.gallery__slider_container', {
  //  init: false,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});

jQuery(document).ready(function(){

    jQuery('.scrollbar_inner').scrollbar();


    $('.app_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.app').toggleClass('app_close');
        $('.app').removeClass('open_gallery');
    });

    // Show result

    $('.nav__group li a').on('click', function(e){
        e.preventDefault();
        $('.source').addClass('active');
    });

    // Открыть описание места
    $('.result').on('click', function(e){
        e.preventDefault();
        $('.place').addClass('active');
        $('.header').addClass('header_place');
        $('.result').removeClass('active');
        $(this).addClass('active');
    });


    // Закрыть панель

    $('.source_hide').on('click touchstart', function(e){
        e.preventDefault();
        $('.source').removeClass('active');
    });

    $('.place_hide').on('click touchstart', function(e){
        e.preventDefault();
        $('.header').removeClass('header_place');
        $('.place').removeClass('active');
        $('.result').removeClass('active');
        $('.app').removeClass('open_gallery');

    });

    // Показать\Скрыть галерею

    $('.place__slider_item a').on('click', function(e){
        e.preventDefault();
        $('.app').addClass('open_gallery');
        slider.update();
    });

    $('.gallery_close').on('click touchstart', function(e){
        e.preventDefault();
        $('.app').removeClass('open_gallery');
    });


    $('.mobile_layer').on('click touchstart', function(e){
        e.preventDefault();
        $('.app').toggleClass('app_close');
    });

/*
    $('body').click(function (event) {

        var w = window.width();

        if (w < 1200) {

            if ($(event.target).closest(".main").length === 0) {

                if ($(event.target).closest(".source").length === 0) {

                    if ($(event.target).closest(".place").length === 0) {

                        if ($(event.target).closest(".header").length === 0) {

                            $(".app").addClass('app_close');
                        }
                    }
                }
            }

        }
    });
*/

    // Выбор радиуса на карте
    (function() {

        $(".selectBox")
            .mouseenter(function() {
                $(this).addClass('open');
            })
            .mouseleave(function(){
                $(this).removeClass('open');
            });

        $('.selectBox__nav input').on('change', function(e){

            var rd = $(this).val();
            var rdText = rd + 'км';
            $(this).closest('.selectBox').find('.selectBox__label span').text(rdText);
            $(this).closest('.selectBox').removeClass('open');
            console.log(rdText);
        });

    }());

});


// Map

var markersData = [
    {
        lat         :       56.246205,     // Широта
        lng         :       43.8964165,    // Долгота
        name        :       "Кафе Милано", // Произвольное название, которое будем выводить в информационном окне
        address     :       "Ул. Волгоградская 19",   // Адрес, который также будем выводить в информационном окне
        phone       :       "+37544 762-11-11",
        distance    :       "800 м~5мин.",
        rate        :       "4,7",
        comment     :       "134",
        worktime    :       "до 00:00",
        photo_01    :       "images/gallery_image.jpg",
        photo_02    :       "images/gallery_image.jpg",
        photo_03    :       "images/gallery_image.jpg",
        photo_04    :       "images/gallery_image.jpg"
    },
    {
        lat         :       56.2763807,
        lng         :       43.94534,
        name        :       "Кафе Милано",
        address     :       "Ул. Волгоградская 19",
        phone       :       "+37544 762-11-11",
        distance    :       "800 м~5мин.",
        rate        :       "4,7",
        comment     :       "134",
        worktime    :       "до 00:00",
        photo_01    :       "images/gallery_image.jpg",
        photo_02    :       "images/gallery_image.jpg",
        photo_03    :       "images/gallery_image.jpg",
        photo_04    :       "images/gallery_image.jpg"
    },
    {
        lat         :       56.3144715,
        lng         :       43.9922894,
        name        :       "Кафе Милано",
        address     :       "Ул. Волгоградская 19",
        phone       :       "+37544 762-11-11",
        distance    :       "800 м~5мин.",
        rate        :       "4,7",
        comment     :       "134",
        worktime    :       "до 00:00",
        photo_01    :       "images/gallery_image.jpg",
        photo_02    :       "images/gallery_image.jpg",
        photo_03    :       "images/gallery_image.jpg",
        photo_04    :       "images/gallery_image.jpg"
    }
];

// Объявляем переменные map и infoWindow за пределами функции initMap,
// тем самым делая их глобальными и теперь мы их можем использовать внутри любой функции, а не только внутри initMap, как это было раньше.
var map, infoWindow;

function initMap() {

    var centerLatLng = new google.maps.LatLng(56.2928515, 43.7866641);
    var mapOptions = {
        center: centerLatLng,
        zoom: 10
    };

    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    // Создаем объект информационного окна и помещаем его в переменную infoWindow
    // Так как у каждого информационного окна свое содержимое, то создаем пустой объект, без передачи ему параметра content

    infoWindow = new google.maps.InfoWindow({
        content: '<div class="gm_content"></div>'
    });
    // Отслеживаем клик в любом месте карты

    google.maps.event.addListener(map, "click", function() {
        // infoWindow.close - закрываем информационное окно.
        infoWindow.close();
    });


    // Перебираем в цикле все координата хранящиеся в markersData
    for (var i = 0; i < markersData.length; i++){
        var latLng      = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
        var name        = markersData[i].name;
        var address     = markersData[i].address;
        var phone       = markersData[i].phone;
        var distance    = markersData[i].distance;
        var rate        = markersData[i].rate;
        var comment     = markersData[i].comment;
        var worktime    = markersData[i].worktime;
        var photo_01    = markersData[i].photo_01;
        var photo_02    = markersData[i].photo_01;
        var photo_03    = markersData[i].photo_01;
        var photo_04    = markersData[i].photo_01;
        // Добавляем маркер с информационным окном
        addMarker(latLng, name, address, phone, distance, rate, comment, worktime, photo_01, photo_02, photo_03, photo_04);
    }
}

//google.maps.event.addDomListener(window, "load", initMap);


// Функция добавления маркера с информационным окном
function addMarker(latLng, name, address, phone, distance, rate, comment, worktime, photo_01, photo_02, photo_03, photo_04) {
    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        title: name
    });
    // Отслеживаем клик по нашему маркеру
    google.maps.event.addListener(marker, "click", function() {
        // contentData - это переменная в которой хранится содержимое информационного окна.

        var contentData = '<div class="mapInfo">' +
            '<div class="mapInfo__gallery">' +
                '<div class="mapInfo__gallery_elem"><a href="#"><img class="img-fluid" src="' + photo_01 + '"></a></div>' +
                '<div class="mapInfo__gallery_elem"><a href="#"><img class="img-fluid" src="' + photo_02 + '"></a></div>' +
                '<div class="mapInfo__gallery_elem"><a href="#"><img class="img-fluid" src="' + photo_03 + '"></a></div>' +
                '<div class="mapInfo__gallery_elem"><a href="#"><img class="img-fluid "src="' + photo_04 + '"></a></div>' +
            '</div>' +
            '<ul class="mapInfo__meta">' +
                '<li>' +
                    '<div class="mapInfo__meta_area">' +
                        '<span>' + distance + '</span>' +
                        '<i class="icon_meta_man"><svg class="ico-svg"  viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg"><use xlink:href="img/sprite_icons.svg#icon_man" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg></i>' +
                    '</div>' +
                '</li>' +
                '<li>' +
                    '<div class="mapInfo__meta_rate">' +
                        '<i class="icon_meta_man"><svg class="ico-svg"  viewBox="0 0 11 9" fill="none" xmlns="http://www.w3.org/2000/svg"><use xlink:href="img/sprite_icons.svg#icon_star" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg></i>' +
                        '<span>' + rate + '</span>' +
                    '</div>' +
                '</li>' +
                '<li>' +
                    '<div class="mapInfo__meta_comments">' +
                        '<i class="icon_meta_man"><svg class="ico-svg"  viewBox="0 0 11 9" fill="none" xmlns="http://www.w3.org/2000/svg"><use xlink:href="img/sprite_icons.svg#icon_bubble" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg></i>' +
                        '<span>' + comment + '</span>' +
                    '</div>' +
                '</li>' +
                '<li><strong>' + worktime  +'</strong></li>' +
            '</ul>' +
            '<h4><strong>' + name + '</strong> ' + address + '</h4>' +
            '<div class="mapInfo__phone">' + phone + '</div>' +
            '</div>';

        // Меняем содержимое информационного окна
        infoWindow.setContent(contentData);
        // Показываем информационное окно
        infoWindow.open(map, marker);
    });
}

